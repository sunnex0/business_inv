import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "business_inv.settings")

django.setup()

from inventories.models import Category, Listing

categories = [
    "Social Business",
    "Big Business",
    "Small Business",
    "Tech Startup",
    "Restaurant",
    "Housing Complex",
    "Construction",
    "Automobile",
    "Fashion"
]

listings = [
    {
        "name": "Apple",
        "description": "A big technology that creates laptops and phone with the sole reason of excellence in satisfying the user's desires",
        "web": "www.apple.com",
        "email": "info@apple.com",
        "phone1": "0902245678",
        "address": "No 29 Brooklyn Street, Ohio United States",
        "views": 5690,
        "types": "Big Business | Tech Startup"

    },
    {
        "name": "Konga",
        "description": "A social entreprenueral ecommerce startup with the aim of making shopping easy",
        "web": "www.konga.com",
        "email": "info@konga.com",
        "phone1": "0992459273",
        "address": "No 6 Leke Crescent, GRA Ikeja",
        "views": 647,
        "types": "Tech Startup"
    },
    {
        "name": "Kingsman Tailors",
        "description": "A fashion company dedicated to fulfilling all your clothing fantasies",
        "web": "www.kingsman.com",
        "email": "info@kingsman.com",
        "phone1": "0224599243",
        "address": "225 Wellington Avenue London",
        "views": 4637,
        "types": "Big Business | Fashion"
    },
    {
        "name": "KFC",
        "description": "We fry chicken at the highest quality with premium taste and flavor to satisfy longing stomach",
        "web": "www.kfc.com",
        "email": "info@kfc.com",
        "phone1": "2343433978",
        "address": "226 Farlough Street ICM Ikeja",
        "views": 245,
        "types": "Small Business| Restaurant"
    },
    {
        "name": "Julius Berger",
        "description": "The biggest construction company ever. We take road construction contracts and bridge construction contracts",
        "web": "www.juliusb.com",
        "email": "info@juliusb.com",
        "phone1": "2934942973",
        "address": "563 Julius Berger Street, California, United States",
        "views": 435,
        "types": "Big Business | Construction"
    },
    {
        "name": "Lape Cooks",
        "description": "A country kitchen investment to feed campus students",
        "web": "www.lape.cooks.com",
        "email": "info@lape.cooks.com",
        "phone1": "08135432343",
        "address": "No 4, Issac John Street, Ikeja Lagos",
        "views": 200,
        "types": "Small Business | Restaurant | Social Business"
    }
]


def create_category(categories):
    for cat in categories:
        category = Category(
            name=cat
        )
        category.save()


def create_listing(listings):
    for listing in listings:
        business = Listing(
            name=listing["name"],
            business_description=listing["description"],
            email=listing["email"],
            phone1=listing["phone1"],
            address=listing["address"],
            views=listing["views"],
            web_address=listing["web"],
            types=listing["types"]

        )
        business.save()

        bus_list = listing["types"].split("|")
        bus_list2 = [b.strip() for b in bus_list]

        for types in bus_list2:
            cat = Category.objects.get(name=types)
            cat.listing.add(business)


create_category(categories)
create_listing(listings)
