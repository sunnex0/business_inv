from django.conf.urls import url

from .views import edit_listing, home, category_details, page_404, listing_list, create_listing, list_details, \
    category_list, category_delete, listing_delete

urlpatterns = [
    url(r'^$', home, name="home"),
    url(r'^listing/(?P<pk>[\d]+)/$', list_details, name="listing"),
    url(r'^category/(?P<pk>[\d]+)/$', category_details, name="category"),
    url(r'^category/delete/(?P<pk>\d+)/$', category_delete, name='category_delete'),
    url(r'^listing/delete/(?P<pk>\d+)/$', listing_delete, name='list_delete'),
    url(r'^create_listing/$', create_listing, name='create_listing'),
    url(r'^edit_listing/(?P<pk>[\d]+)/$', edit_listing, name='edit_listing'),
    url(r'^listing_list/$', listing_list, name='listing_list'),
    url(r'^category_list/$', category_list, name='category_list'),
    url(r'^404/$', page_404, name='404'),
]
