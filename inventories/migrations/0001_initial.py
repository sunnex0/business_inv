# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-12-13 07:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Listing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('business_description', models.CharField(max_length=1000)),
                ('web_address', models.URLField(blank=True, max_length=1000)),
                ('email', models.EmailField(blank=True, max_length=300)),
                ('phone1', models.CharField(blank=True, max_length=15)),
                ('phone2', models.CharField(blank=True, max_length=15)),
                ('address', models.CharField(blank=True, max_length=1000)),
                ('views', models.IntegerField(default=0)),
                ('types', models.CharField(blank=True, max_length=30)),
            ],
        ),
        migrations.AddField(
            model_name='category',
            name='listing',
            field=models.ManyToManyField(blank=True, to='inventories.Listing'),
        ),
    ]
