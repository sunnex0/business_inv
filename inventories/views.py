from django.shortcuts import render, Http404
from .models import Category, Listing
from django.http import HttpResponseRedirect
from django.contrib import messages
from .forms import ListingForm

""" Home View """


def home(request):
    business_list = Listing.objects.all().order_by('-views')[
                    0:5]  # get the top five business list based on the number of views
    categories = Category.objects.all()[0:5]  # get the top five categories
    context = {
        "business": business_list,
        "categories": categories
    }
    return render(request, "home.html", context)


# Home View Ends

""" Listing List View. This view renders the listing list template with a search """


def listing_list(request):
    business_list = Listing.objects.all().order_by('-views')[0:10]
    context = {
        'business_list': business_list
    }
    return render(request, 'listing_list.html', context)


def category_list(request):
    categories = Category.objects.all()[0:10]
    context = {
        'categories': categories
    }
    return render(request, 'category_list.html', context)


def list_details(request, pk):
    try:
        listing = Listing.objects.get(pk=pk)
        list_cat = listing.category_set.all()
        context = {
            "listing": listing,
            "list_cat": list_cat
        }
        response = render(request, 'list_details.html', context)
    except Listing.DoesNotExist:
        response = HttpResponseRedirect('/404/')
    # listing = get_object_or_404(Listing, pk=pk)
    # print(listing)

    return response


def category_details(request, pk):
    category = Category.objects.get(pk=pk)
    category_list = category.listing.all()
    context = {
        "category": category,
        "category_list": category_list
    }
    return render(request, 'category_details.html', context)


def category_delete(request, pk):
    Cats = Category.objects.get(pk=pk)
    Cats.delete()
    messages.success(request, Cats.name + " Category has been Deleted")

    return HttpResponseRedirect('/')


def listing_delete(request, pk):
    List = Listing.objects.get(pk=pk)
    List.delete()
    messages.success(request, List.name + " List has been Deleted")
    return HttpResponseRedirect('/')


def create_listing(request):
    if request.method == 'POST':
        form = ListingForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            business_description = form.cleaned_data['business_description']
            web_address = form.cleaned_data['web_address']
            email = form.cleaned_data['email']
            phone1 = form.cleaned_data['phone1']
            phone2 = form.cleaned_data['phone2']
            address = form.cleaned_data['address']
            views = form.cleaned_data['views']
            types = form.cleaned_data['types']

            listing = Listing(
                name=name,
                business_description=business_description,
                web_address=web_address,
                email=email,
                phone1=phone1,
                phone2=phone2,
                address=address,
                views=views,
                types=types
            )
            listing.save()

            return HttpResponseRedirect('/listing/{0}'.format(listing.pk))
    else:
        form = ListingForm()

    return render(request, 'create_listing.html', {'form': form})


def edit_listing(request, pk):
    # listing =
    listing = Listing.objects.get(pk=pk)
    if request.method == 'POST':
        form = ListingForm(request.POST)
        if form.is_valid():
            listing.name = form.cleaned_data['name']
            listing.business_description = form.cleaned_data['business_description']
            listing.web_address = form.cleaned_data['web_address']
            listing.email = form.cleaned_data['email']
            listing.phone1 = form.cleaned_data['phone1']
            listing.phone2 = form.cleaned_data['phone2']
            listing.address = form.cleaned_data['address']
            listing.views = form.cleaned_data['views']
            listing.types = form.cleaned_data['types']
            listing.save()
            return HttpResponseRedirect('/listing/{0}'.format(listing.pk))
    else:
        form = ListingForm(
            initial={
                'name': listing.name,
                'business_description': listing.business_description,
                'web_address': listing.web_address,
                'email': listing.email,
                'phone1': listing.phone1,
                'phone2': listing.phone2,
                'address': listing.address,
                'views': listing.views,
                'types': listing.types
            }
        )

    return render(request, 'create_listing.html', {'form': form})


# def create_category(request):
#     if request.method == 'POST':
#         form = CategoryForm(request.POST)


def login(request):
    return render(request, "login.html", {})


def page_404(request):
    return render(request, '404_page.html', {})
