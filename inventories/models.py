from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)
    listing = models.ManyToManyField('Listing', blank=True)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Listing(models.Model):
    name = models.CharField(max_length=255, unique=True)
    business_description = models.CharField(max_length=1000)
    web_address = models.URLField(max_length=1000, blank=True)
    email = models.EmailField(max_length=300, blank=True)
    phone1 = models.CharField(max_length=15, blank=True, )
    phone2 = models.CharField(max_length=15, blank=True)
    address = models.CharField(max_length=1000, blank=True)
    views = models.IntegerField(default=0)
    types = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.name
