from django import forms


class ListingForm(forms.Form):
    name = forms.CharField(required=True, max_length=255,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}))
    business_description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Business Description'}), max_length=255,
        required=True)
    web_address = forms.CharField(required=False,
                                  widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Web Address'}))
    email = forms.EmailField(required=False,
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    phone1 = forms.CharField(label='Phone', required=False,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Phone'}))
    phone2 = forms.CharField(label='Alternate Phone', required=False,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Alternate Phone'}))
    address = forms.CharField(label='Location', required=False,
                              widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Address'}))
    views = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Views'}))
    types = forms.CharField(label='Categories', required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Categories'}))
    field_order = ['name', 'business_description', 'phone1', 'phone2', 'email']


class CategoryForm(forms.Form):
    name = forms.CharField(required=True, max_length=255,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}))
    listing = forms.ChoiceField()
